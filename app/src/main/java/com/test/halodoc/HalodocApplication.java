package com.test.halodoc;

import android.app.Application;

/**
 * Created by Arpit on 18/12/18.
 */

public class HalodocApplication extends Application {

    private static HalodocApplication halodocApplication;

    public synchronized static HalodocApplication getInstance() {
        return halodocApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        halodocApplication = HalodocApplication.this;
    }
}
