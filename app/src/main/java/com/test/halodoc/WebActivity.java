package com.test.halodoc;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.test.halodoc.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.test.halodoc.main.MainActivity.WEB_URL;

public class WebActivity extends AppCompatActivity {

    @BindView(R.id.wv_news)
    WebView wvNews;

    @BindView(R.id.web_progress_bar)
    ProgressBar progressBar;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);

        wvNews.getSettings().setJavaScriptEnabled(true);

        if (getIntent() != null) {
            String webUrl = getIntent().getStringExtra(WEB_URL);
            if (AppUtils.isNotEmpty(webUrl)) {

                wvNews.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        progressBar.setVisibility(View.GONE);

                    }
                });
                wvNews.loadUrl(webUrl);
            }
        } else {
            AppUtils.showToast(getResources().getString(R.string.nothing_load));
            finish();
        }

    }
}
