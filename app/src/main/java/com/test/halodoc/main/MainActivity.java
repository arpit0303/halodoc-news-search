package com.test.halodoc.main;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.test.halodoc.R;
import com.test.halodoc.model.NewsBean;
import com.test.halodoc.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    MainContract.Presenter presenter;

    public static final String WEB_URL = "webUrl";

    @BindView(R.id.til_news_category)
    TextInputLayout tilNewsCategory;

    @BindView(R.id.rv_news)
    RecyclerView rvNews;

    @BindView(R.id.view)
    View progressView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MainPresenter(this);

    }

    @OnClick(R.id.btn_go)
    public void fetchNews() {
        hideKeyboard();
        Editable etNews = tilNewsCategory.getEditText().getText();
        if (etNews != null && AppUtils.isNotEmpty(etNews.toString())) {
            showProgressBar();
            presenter.getNews(etNews.toString());
        }
    }

    @Override
    public void getNewsSuccess(NewsBean bean) {
        hideProgressBar();
        LinearLayoutManager manager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rvNews.setLayoutManager(manager);

        NewsAdapter adapter = new NewsAdapter(bean.getHits());
        rvNews.setAdapter(adapter);

    }

    private void showProgressBar() {
        progressView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void hideProgressBar() {
        progressView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void getNewsFailure(String errorMessage) {
        hideProgressBar();
        AppUtils.showToast(errorMessage);
    }

    @Override
    public Context getContext() {
        return MainActivity.this;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(MainActivity.this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
