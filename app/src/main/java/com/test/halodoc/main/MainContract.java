package com.test.halodoc.main;

import android.content.Context;

import com.test.halodoc.model.NewsBean;

/**
 * Created by Arpit on 18/12/18.
 */

public interface MainContract {

    interface View {

        void getNewsSuccess(NewsBean bean);

        void getNewsFailure(String errorMessage);

        Context getContext();
    }

    interface Presenter {
        void getNews(String query);
    }
}
