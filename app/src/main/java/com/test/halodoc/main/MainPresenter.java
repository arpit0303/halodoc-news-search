package com.test.halodoc.main;

import android.support.annotation.NonNull;

import com.test.halodoc.R;
import com.test.halodoc.model.NewsBean;
import com.test.halodoc.rest.RestRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.test.halodoc.Commons.SUCCESS_CODE;

/**
 * Created by Arpit on 18/12/18.
 */

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;

    MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void getNews(String query) {

        Call<NewsBean> newsCall = RestRequestBuilder.getClient().getNews(query);
        newsCall.enqueue(new Callback<NewsBean>() {
            @Override
            public void onResponse(@NonNull Call<NewsBean> call, @NonNull Response<NewsBean> response) {
                if (view != null) {
                    if (response.code() == SUCCESS_CODE) {
                        if (response.body() != null) {
                            view.getNewsSuccess(response.body());
                        } else {
                            view.getNewsFailure(view.getContext().getResources().getString(R.string.no_news));
                        }
                    } else {
                        view.getNewsFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NewsBean> call, @NonNull Throwable t) {
                if (view != null) {
                    view.getNewsFailure(t.getMessage());
                }
            }
        });
    }
}
