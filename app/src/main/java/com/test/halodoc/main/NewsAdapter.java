package com.test.halodoc.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.halodoc.R;
import com.test.halodoc.WebActivity;
import com.test.halodoc.model.Hit;

import java.util.List;

import static com.test.halodoc.main.MainActivity.WEB_URL;

/**
 * Created by Arpit on 18/12/18.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private Context context;
    private List<Hit> hitList;

    NewsAdapter(List<Hit> hitList) {
        this.hitList = hitList;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_news, viewGroup, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, int position) {
        final Hit hitItem = hitList.get(position);
        newsViewHolder.newsTitle.setText(hitItem.getTitle());
        newsViewHolder.newsAuthor.setText(hitItem.getAuthor());
        newsViewHolder.newsDate.setText(hitItem.getCreatedAt());

        newsViewHolder.clNewsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(context, WebActivity.class);
                webIntent.putExtra(WEB_URL, hitItem.getUrl());
                context.startActivity(webIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hitList.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout clNewsItem;
        TextView newsTitle, newsAuthor, newsDate;

        NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            clNewsItem = itemView.findViewById(R.id.news_item);
            newsTitle = itemView.findViewById(R.id.news_title);
            newsAuthor = itemView.findViewById(R.id.news_author);
            newsDate = itemView.findViewById(R.id.news_date);
        }
    }
}
