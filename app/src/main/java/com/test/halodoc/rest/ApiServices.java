package com.test.halodoc.rest;

import com.test.halodoc.model.NewsBean;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Arpit on 18/12/18.
 */

public interface ApiServices {

    @GET("api/v1/search")
    Call<NewsBean> getNews(@Query("query") String query);
}
