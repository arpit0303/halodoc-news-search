package com.test.halodoc.utils;

import android.util.Log;
import android.widget.Toast;

import com.test.halodoc.BuildConfig;
import com.test.halodoc.HalodocApplication;

/**
 * Created by Arpit on 18/12/18.
 */

public class AppUtils {

    public static boolean isNotEmpty(String str) {
        return str != null && !str.trim().isEmpty();
    }

    public static void showToast(String msg) {
        Toast.makeText(HalodocApplication.getInstance(), msg, Toast.LENGTH_SHORT).show();
    }

    public static void showLogs(String tag, String msg) {
        if(!BuildConfig.IS_PROD) {
            Log.i(tag, msg);
        }
    }
}
